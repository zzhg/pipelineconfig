#!/bin/sh
set -e # fail fast
set -x # print commands


git clone TestReport report
git config --global user.email "mr_zg_001@163.com"
git config --global user.name "zzhg"

cd report
report="Mosc3.2_API-LiftShift_"`date +%Y%m%d`
#robot --outputdir ./report -t "SC02-Primaryuser_TC01_Enrolment System_Requesting MBB Access Tokens_Request System AT - FAW" ./Mosc3.2_API-LiftShift\(FAW\)-TUI-v1.2.5/SC02-MBBC-Primary_user_enrollment.robot 
#robot --outputdir ./report -t "SC02-Primaryuser_TC02_Enrolment System_Requesting MBB Access Tokens_Request System AT + User" ./Mosc3.2_API-LiftShift\(FAW\)-TUI-v1.2.5/SC02-MBBC-Primary_user_enrollment.robot 
robot --outputdir ./${report}/  ../RobotTestScripts/Mosc3.2_API-LiftShift\(FAW\)-TUI-v1.2.5/SC02-MBBC-Primary_user_enrollment.robot > /dev/null &

echo "测试任务执行中，请稍候。。。"
#wait命令会等待上一个进程任务运行结束后，再继续往下运行
wait
echo "测试脚本执行完毕！"
git add .
git commit -m "update test report"
#git push -u origin master
